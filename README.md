# Monizze payment library (PHP)

![Monizze](https://www.monizze.be/content/themes/monizze/dist/img/logo_monizze.svg)

Library written in PHP to ease the integration of Monizze payment solution as a button or through the payment API.

## Install it

### Composer

Install extension using [composer](https://getcomposer.org):

Add the repository to your composer file (see https://getcomposer.org/doc/05-repositories.md#loading-a-package-from-a-vcs-repository):

```json
    "repositories": [
        {
            "type": "vcs",
            "url": "https://gitlab.com/monizze/payment-library/"
        }
    ],
```

Then install it:

```shell
composer require monizze/payment_lib
```

### Without composer

Download the last version of [https://gitlab.com/monizze/payment-library/tags](the release).

## Usage

In all example, the `$monizzeClient` is an instance of `\MonizzeLib\Payment\Client`


#### Create the signature of a PayButton

```php
// $orderReference is your reference for the given order related to this payment
// $amount is the total amount in eurocent of this payment
// $productCode is the code related to the product to use to pay (by example PayButton::GIFT)

use MonizzeLib\Payment\Container\PayButton;
use MonizzeLib\Payment\Client;

$button = new PayButton($orderReference, $amount, $productCode);
$btnData = $monizzeClient->fillRequestData($button);
$signature = $monizzeClient->signData($button, $btnData);
```


#### Check the return of a pay button

```php
use MonizzeLib\Payment\Message\OrderStatusRequest;
use MonizzeLib\Payment\Message\PayButtonResponse;
use MonizzeLib\Payment\Message\OrderStatusResponse;

$response = new PayButtonResponse($_GET['MREF'], $_GET['STATUS'], null);
if (!$monizzeClient->validateSignatureOnSignedData($_GET['SIGN'], $response))
{
	echo 'The return from Monizze is invalid.';
	exit();
}

if ($response->getPaymentStatus() == PayButtonResponse::STATUS_ACCEPTED)
{
    echo 'Congrats, the payment ha been validated';
};
```

#### Verify the status of a payment asynchronously

```php
// $orderReference is your reference for the given order related to this payment
// $amount is the total amount in eurocent of this payment

use \MonizzeLib\Payment\Message\OrderStatusRequest;

$orderStatusRequest = new OrderStatusRequest($orderReference), $amount);
$statusResponse = $monizzeClient->doRequest($orderStatusRequest);
if ($statusResponse->getStatus() == PayButtonResponse::STATUS_ACCEPTED)
{
    echo 'Congrats, the payment ha been validated';
};
```

### PSR note
The library is based on [PSR-7](https://www.php-fig.org/psr/psr-7/) to make the HTTP call
so you can use any [library](https://packagist.org/search/?tags=psr-7) that is compatible with.

[PSR-18](https://www.php-fig.org/psr/psr-18/) & [PSR-17](https://www.php-fig.org/psr/psr-17/) should
help finalize the standardization. But during this transition time, the `MonizzeLib\Payment\AdapterPsr\ClientInterface` will be present.

### Don't want to use Guzzle?
Just implement the `MonizzeLib\Payment\AdapterPsr\ClientInterface` and that's all.

## Requirement

### Usage
To use it, you need to have three information provided by Monizze:
* Your merchant ID (MID).
* Your key in, to sign the data you will send.
* Your key out, to verify the data you will received.

To obtain them you need to contact [Monizze](mailto:info@monizze.be)

### Technical
You need to have at least php 7.1.
For php 5.6, use the branch named php_5-6 to get the appropriate version.
