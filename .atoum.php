<?php

use mageekguy\atoum\reports\coverage,
	mageekguy\atoum\writers\std,
	mageekguy\atoum\telemetry;

if (getenv('ATOUM_COVERAGE_PATH'))
{
	$script->enableBranchAndPathCoverage();
}

// ignore stubs
$script->noCodeCoverageForClasses('stubs\AbstractResponseToRequest');

$runner->addTestsFromDirectory(__DIR__ . '/test/units');

$script->addDefaultReport();

# telemetry report
$telemetry = new telemetry\report();
$telemetry->addWriter(new std\out());
$telemetry->readProjectNameFromComposerJson(__DIR__ . '/composer.json');
$runner->addReport($telemetry);

if (getenv('TRAVIS'))
{
	$travisReport = new reports\realtime\cli\travis();
	$travisReport->addWriter(new std\out());
	$runner->addReport($travisReport);

	return;
}

# html coverage
$coverage = new coverage\html();
$coverage->addWriter(new std\out());
$coverage->setOutPutDirectory(__DIR__ . '/test/reports/coverage');
$runner->addReport($coverage);
