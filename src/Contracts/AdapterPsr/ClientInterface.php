<?php

namespace MonizzeLib\Payment\Contracts\AdapterPsr;

use Psr\Http\Message\ResponseInterface;

interface ClientInterface
{
	public function postData($url, array $header, array $data): ResponseInterface;
}
