<?php

namespace MonizzeLib\Payment\Contracts\Message;

interface WithSignedData
{
	const SIGN_TO_MONIZZE = 'in';
	const SIGN_FROM_MONIZZE = 'out';

	/**
	 * Return the name of the parameters that will be signed
	 * @return array
	 */
	public function getDataNameToSign(): array;

	/**
	 * Return the known parameters for the message
	 * @return array
	 */
	public function getData(): array;

	/**
	 * Get the direction for the signed data
	 * @return string
	 */
	public function getDirection(): string;
}
