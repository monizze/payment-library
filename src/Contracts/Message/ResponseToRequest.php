<?php

namespace MonizzeLib\Payment\Contracts\Message;

use MonizzeLib\Payment\Exception\InvalidResponse;
use Psr\Http\Message\ResponseInterface;

interface ResponseToRequest extends Response
{
	/**
	 * @param ResponseInterface $response
	 *
	 * @throws InvalidResponse
	 * @throws \DomainException
	 * @return ResponseToRequest
	 */
	public static function createFromResponse(ResponseInterface $response): ResponseToRequest;

	public function isResponseValid(ResponseInterface $response): bool;

	/**
	 * @param ResponseInterface $response
	 *
	 * @return string
	 */
	public function extractContentFromResponse(ResponseInterface $response): string;
}
