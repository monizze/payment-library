<?php

namespace MonizzeLib\Payment\Contracts\Message;

use Psr\Http\Message\ResponseInterface;

interface Request extends WithSignedData
{
	/**
	 * Return the endpoint where to send the request
	 * @return string
	 */
	public function getEndpoint(): string;

	/**
	 * Create the response message from the response to the request
	 *
	 * @param ResponseInterface $response
	 *
	 * @return ResponseToRequest
	 */
	public function createResponse(ResponseInterface $response): ResponseToRequest;
}
