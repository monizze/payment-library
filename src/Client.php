<?php

namespace MonizzeLib\Payment;

use MonizzeLib\Payment\Container\Configuration;
use MonizzeLib\Payment\Contracts\AdapterPsr\ClientInterface;
use MonizzeLib\Payment\Contracts\Message\WithSignedData;
use MonizzeLib\Payment\Exception\InvalidRequestData;
use MonizzeLib\Payment\Contracts\Message\Request;
use MonizzeLib\Payment\Contracts\Message\ResponseToRequest;

class Client
{
	const PARAM_MID = 'MID';
	const PARAM_KEY = 'KEY';
	const PARAM_SIGN = 'SIGN';

	/**
	 * @var Configuration
	 */
	protected $configuration;

	/**
	 * @var ClientInterface
	 */
	protected $httpClient;

	public function __construct(Configuration $configuration, ClientInterface $httpClient)
	{
		$this->configuration = $configuration;
		$this->httpClient = $httpClient;
	}

	public function doRequest(Request $request): ResponseToRequest
	{
		$data = $this->fillRequestData($request);
		$data[ self::PARAM_SIGN ] = $this->signData($request, $data);

		// create request && send it
		$url = 'https://www.monizze.be/en/' . $request->getEndpoint();
		$headers = [
			'Accept' => 'application/xml',
		];
		$responseHTTP = $this->httpClient->postData($url, $headers, $data);

		// process the response
		return $request->createResponse($responseHTTP);
	}

	public function validateSignatureOnSignedData(string $signatureReceived, WithSignedData $withSignedData): bool
	{
		return hash_equals($this->signData($withSignedData), $signatureReceived);
	}

	public function signData(WithSignedData $withSignedData, array $data = array()): string
	{
		// prepare the data to sign
		$signData = array();
		$data = empty($data) ? $withSignedData->getData() : $data;
		foreach ($withSignedData->getDataNameToSign() as $item)
		{
			if (!isset($data[ $item ]))
			{
				throw new InvalidRequestData('The ' . $item . ' is missing, so it can\'t be signed.');
			}

			$signData[] = $data[ $item ];
		}

		// prepare the signature
		if ($withSignedData->getDirection() == WithSignedData::SIGN_TO_MONIZZE)
		{
			$signData[] = $this->configuration->getKeyIn();
			$algo = $this->configuration->getSignAlgo();
		}
		else
		{
			$signData[] = $this->configuration->getKeyOut();
			$algo = 'sha1';
		}

		// sign it
		return hash($algo, implode('|', $signData));
	}

	public function fillRequestData(WithSignedData $request): array
	{
		// getting the data
		$data = $request->getData();

		// replace MID
		if (isset($data[ self::PARAM_MID ]) && $data[ self::PARAM_MID ] == self::PARAM_MID)
		{
			$data[ self::PARAM_MID ] = $this->configuration->getMerchantId();
		}

		return $data;
	}
}
