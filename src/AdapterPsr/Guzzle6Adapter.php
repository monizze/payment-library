<?php

namespace MonizzeLib\Payment\AdapterPsr;

use MonizzeLib\Payment\Contracts\AdapterPsr\ClientInterface;
use Psr\Http\Message\ResponseInterface;

class Guzzle6Adapter implements ClientInterface
{
	public function postData($url, array $header, array $data): ResponseInterface
	{
		$client = new \GuzzleHttp\Client();
		$response = $client->post($url, [
			'headers' => $header,
			'form_params' => $data,
		]);

		return $response;
	}
}
