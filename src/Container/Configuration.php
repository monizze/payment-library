<?php

namespace MonizzeLib\Payment\Container;

class Configuration
{
	/**
	 * The key to sign all the communication to send to Monizze
	 * @var string
	 */
	protected $keyIn;

	/**
	 * The key to validate the Monizze's communication
	 * @var string
	 */
	protected $keyOut;

	/**
	 * Your uniq identifier at Monizze
	 * @var int
	 */
	protected $merchantId;

	/**
	 * @var string
	 */
	protected $signAlgo;

	public function __construct(string $keyIn, string $keyOut, int $merchantId, string $signAlgo)
	{
		$this->keyIn = $keyIn;
		$this->keyOut = $keyOut;
		$this->merchantId = $merchantId;
		$this->signAlgo = $signAlgo;
	}

	/**
	 * @return string
	 */
	public function getKeyIn(): string
	{
		return $this->keyIn;
	}

	/**
	 * @return string
	 */
	public function getKeyOut(): string
	{
		return $this->keyOut;
	}

	/**
	 * @return int
	 */
	public function getMerchantId(): int
	{
		return $this->merchantId;
	}

	/**
	 * @return string
	 */
	public function getSignAlgo(): string
	{
		return $this->signAlgo;
	}
}
