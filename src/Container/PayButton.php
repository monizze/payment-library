<?php

namespace MonizzeLib\Payment\Container;

use MonizzeLib\Payment\Client;
use MonizzeLib\Payment\Contracts\Message\WithSignedData;

class PayButton implements WithSignedData
{
	const EMV = 200001;
	const ECO = 200002;
	const GIFT = 200003;

	/**
	 * the reference of *your* order, it should be uniq
	 * @var string
	 */
	public $merchantOrderReference;

	/**
	 * the amount in eurocent
	 * @var int
	 */
	public $amountInEurocent;

	/**
	 * The type of voucher
	 * @var int
	 * @see constant EMV, ECO and GIFT
	 */
	public $typeOfVoucher;

	/**
	 * The name of your shop
	 * Can be an empty string
	 * @var string
	 */
	public $shopName = '';

	/**
	 * The email of your user|customer, the person who order something.
	 * Can be an empty string
	 * @var string
	 */
	public $contactEmail = '';

	/**
	 * Return URL if the payment is accepted
	 * If none is provided, it will redirect to the url Monizze as in it's configuration
	 * of your merchant account
	 * @var string
	 */
	public $urlOfPaymentAccepted = '';

	/**
	 * Return URL if the payment is refused
	 * If none is provided, it will redirect to the url Monizze as in it's configuration
	 * of your merchant account
	 * @var string
	 */
	public $urlOfPaymentDeclined = '';

	/**
	 * Return URL if the payment is canceled
	 * If none is provided, it will redirect to the url Monizze as in it's configuration
	 * of your merchant account
	 * @var string
	 */
	public $urlOfPaymentCanceled = '';

	public function __construct(string $orderReference, int $amountInEurocent, int $typeOfVoucher)
	{
		$this->merchantOrderReference = $orderReference;
		$this->typeOfVoucher = $typeOfVoucher;
		$this->amountInEurocent = $amountInEurocent;
	}

	public function getDataNameToSign(): array
	{
		return [
			Client::PARAM_MID,
			'MREF',
			'AMOUNT',
			'TYPE',
			'EMAIL',
			'SHOP',
			'URL_ACCEPTED',
			'URL_CANCELED',
			'URL_DECLINED',
		];
	}

	public function getData(): array
	{
		return [
			Client::PARAM_MID => Client::PARAM_MID,
			'MREF' => $this->merchantOrderReference,
			'AMOUNT' => $this->amountInEurocent,
			'TYPE' => $this->typeOfVoucher,
			'EMAIL' => $this->contactEmail,
			'SHOP' => $this->shopName,
			'URL_ACCEPTED' => $this->urlOfPaymentAccepted,
			'URL_CANCELED' => $this->urlOfPaymentCanceled,
			'URL_DECLINED' => $this->urlOfPaymentDeclined,
		];
	}

	public function getDirection(): string
	{
		return WithSignedData::SIGN_TO_MONIZZE;
	}
}
