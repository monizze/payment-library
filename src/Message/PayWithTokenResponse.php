<?php

namespace MonizzeLib\Payment\Message;

use MonizzeLib\Payment\Client;
use MonizzeLib\Payment\Contracts\Message\ResponseToRequest;
use MonizzeLib\Payment\Contracts\Message\WithSignedData;
use MonizzeLib\Payment\Exception\InvalidResponse;
use MonizzeLib\Payment\Exception\OrderStatusError;
use MonizzeLib\Payment\Exception\OrderStatusInvalid;
use Psr\Http\Message\ResponseInterface;

/**
 * This class handle the response from a PayWithTokenRequest
 */
class PayWithTokenResponse extends AbstractResponseToRequest implements WithSignedData
{
	/**
	 * @var string
	 */
	protected $status;

	/**
	 * @var string
	 */
	protected $returnedStatus;

	/**
	 * @var string
	 */
	protected $reasonForDecline;

	/**
	 * the reference of *your* order
	 * @var string
	 */
	protected $merchantOrderReference;

	public static function createFromResponse(ResponseInterface $response): ResponseToRequest
	{
		$self = new self();
		$self->responseProcess($response);
		return $self;
	}

	public function jsonToResponse(\stdClass $data): void
	{
		$this->returnedStatus = $data->STATUS;
		switch ($this->returnedStatus)
		{
			case 'ACCEPTED':
				$status = PayButtonResponse::STATUS_ACCEPTED;
				break;
			case 'DECLINED':
				$status = PayButtonResponse::STATUS_DECLINED;
				break;
			default:
				throw new OrderStatusInvalid('The status is not matchable with any known status');
		}
		$this->status = $status;
		$this->reasonForDecline = $data->REASON;
		$this->merchantOrderReference = $data->MREF;
	}

	public function getStatus(): string
	{
		return $this->status;
	}

	public function getReturnedStatus(): string
	{
		return $this->returnedStatus;
	}

	public function getReasonForDecline(): string
	{
		return $this->reasonForDecline;
	}

	public function getMerchantOrderReference(): string
	{
		return $this->merchantOrderReference;
	}

	/**
	 * @param ResponseInterface $response
	 *
	 * @throws InvalidResponse
	 * @throws OrderStatusError
	 * @throws OrderStatusInvalid
	 * @throws \MonizzeLib\Payment\Exception\InvalidJSONResponse
	 */
	protected function responseProcess(ResponseInterface $response): void
	{
		if (!$this->isResponseValid($response))
		{
			throw new InvalidResponse('Invalid response', $response);
		}

		$content = $this->extractContentFromResponse($response);
		$dom = $this->responseToJson($content);
		$this->jsonToResponse($dom);
	}

	/**
	 * Return the name of the parameters that will be signed
	 *
	 * @return array
	 */
	public function getDataNameToSign(): array
	{
		return [
			'MREF',
			'STATUS',
		];
	}

	/**
	 * Return the known parameters for the message
	 *
	 * @return array
	 */
	public function getData(): array
	{
		return [
			'MREF' => $this->merchantOrderReference,
			'STATUS' => $this->returnedStatus,
		];
	}

	/**
	 * Get the direction for the signed data
	 *
	 * @return string
	 */
	public function getDirection(): string
	{
		return self::SIGN_FROM_MONIZZE;
	}
}
