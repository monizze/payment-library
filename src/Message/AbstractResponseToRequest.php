<?php

namespace MonizzeLib\Payment\Message;

use MonizzeLib\Payment\Contracts\Message\ResponseToRequest;
use MonizzeLib\Payment\Exception\InvalidJSONResponse;
use MonizzeLib\Payment\Exception\InvalidXmlResponse;
use Psr\Http\Message\ResponseInterface;

abstract class AbstractResponseToRequest implements ResponseToRequest
{
	public function isResponseValid(ResponseInterface $response): bool
	{
		if ($response->getStatusCode() != 200)
		{
			return false;
		}

		$response->getBody()->rewind();
		if ($response->getBody()->getContents())
		{
			return true;
		}

		return false;
	}

	/**
	 * @param ResponseInterface $response
	 *
	 * @return string
	 */
	public function extractContentFromResponse(ResponseInterface $response): string
	{
		$response->getBody()->rewind();
		$content = $response->getBody()->getContents();

		return $content;
	}

	/**
	 * @param string $content
	 *
	 * @return \SimpleXMLElement
	 * @throws InvalidXmlResponse
	 */
	public function responseToXml(string $content): \SimpleXMLElement
	{
		$parentErrorManagement = libxml_use_internal_errors();
		libxml_use_internal_errors(true);
		$dom = simplexml_load_string($content);
		$errors = libxml_get_errors();
		libxml_use_internal_errors($parentErrorManagement);

		if (!empty($errors))
		{
			throw new InvalidXmlResponse('Impossible to read correctly the response', $errors);
		}

		return $dom;
	}

	public function responseToJson(string $content): \stdClass
	{
		$data = json_decode($content, false);
		if (!$data)
		{
			throw new InvalidJSONResponse('Impossible to read correctly the response', $content);
		}

		return $data;
	}
}
