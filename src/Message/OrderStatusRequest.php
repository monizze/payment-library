<?php

namespace MonizzeLib\Payment\Message;

use MonizzeLib\Payment\Client;
use MonizzeLib\Payment\Contracts\Message\Request;
use MonizzeLib\Payment\Contracts\Message\ResponseToRequest;
use MonizzeLib\Payment\Contracts\Message\WithSignedData;
use Psr\Http\Message\ResponseInterface;

/**
 * This will request the status of a payment matching the reference
 */
class OrderStatusRequest implements Request
{
	protected const URL_PART = 'online/pay/order-status';

	/**
	 * @var string
	 */
	protected $orderReference;

	/**
	 * @var int
	 */
	protected $amountInEurocent;

	public function __construct(string $orderReference, int $amountInEurocent)
	{
		$this->orderReference = $orderReference;
		$this->amountInEurocent = $amountInEurocent;
	}

	public function getDataNameToSign(): array
	{
		return [
			Client::PARAM_MID,
			'MREF',
			'AMOUNT',
		];
	}

	public function getData(): array
	{
		return [
			Client::PARAM_MID => Client::PARAM_MID,
			'MREF' => $this->orderReference,
			'AMOUNT' => $this->amountInEurocent,
		];
	}

	public function getEndpoint(): string
	{
		return self::URL_PART;
	}

	public function createResponse(ResponseInterface $response): ResponseToRequest
	{
		return OrderStatusResponse::createFromResponse($response);
	}

	public function getDirection(): string
	{
		return WithSignedData::SIGN_TO_MONIZZE;
	}
}
