<?php

namespace MonizzeLib\Payment\Message;

use MonizzeLib\Payment\Contracts\Message\ResponseToRequest;
use MonizzeLib\Payment\Exception\InvalidResponse;
use MonizzeLib\Payment\Exception\OrderStatusError;
use MonizzeLib\Payment\Exception\OrderStatusInvalid;
use Psr\Http\Message\ResponseInterface;

/**
 * This class handle the response from a OrderStatusRequest
 */
class OrderStatusResponse extends AbstractResponseToRequest
{
	/**
	 * @var string
	 */
	protected $status;

	/**
	 * @var string
	 */
	protected $returnedStatus;

	public static function createFromResponse(ResponseInterface $response): ResponseToRequest
	{
		$self = new self();
		$self->responseProcess($response);
		return $self;
	}

	public function xmlToResponse(\SimpleXMLElement $dom): void
	{
		if (!isset($dom->status))
		{
			switch ('' . $dom)
			{
				case OrderStatusError::ORDER_NOT_FOUND:
				case OrderStatusError::DATA_INTEGRITY_FAILED:
				case OrderStatusError::MERCHANT_NOT_FOUND:
				case OrderStatusError::FORM_DATA_MISSING:
				case OrderStatusError::NO_POST_DATA:
					throw new OrderStatusError('' . $dom);
				default:
					throw new OrderStatusInvalid('No status provided');
			}
		}

		$this->returnedStatus = trim('' . $dom->status);

		$this->returnedStatusToStatus();
	}

	public function getStatus(): string
	{
		return $this->status;
	}

	public function getReturnedStatus(): string
	{
		return $this->returnedStatus;
	}

	/**
	 * @param ResponseInterface $response
	 *
	 * @throws InvalidResponse
	 * @throws OrderStatusError
	 * @throws OrderStatusInvalid
	 * @throws \MonizzeLib\Payment\Exception\InvalidXmlResponse
	 */
	protected function responseProcess(ResponseInterface $response): void
	{
		if (!$this->isResponseValid($response))
		{
			throw new InvalidResponse('Invalid response', $response);
		}

		$content = $this->extractContentFromResponse($response);
		$dom = $this->responseToXml($content);
		$this->xmlToResponse($dom);
	}

	protected function returnedStatusToStatus(): void
	{
		switch ($this->returnedStatus)
		{
			case 'INITIALIZED':
			case 'CANCELLED':
				$status = PayButtonResponse::STATUS_CANCELLED;
				break;
			case 'ACCEPTED':
				$status = PayButtonResponse::STATUS_ACCEPTED;
				break;
			case 'DECLINED':
				$status = PayButtonResponse::STATUS_DECLINED;
				break;
			default:
				throw new OrderStatusInvalid('The status is not matchable with any known status');
		}
		$this->status = $status;
	}
}
