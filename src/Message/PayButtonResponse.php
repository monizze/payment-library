<?php

namespace MonizzeLib\Payment\Message;

use MonizzeLib\Payment\Client;
use MonizzeLib\Payment\Contracts\Message\Response;
use MonizzeLib\Payment\Contracts\Message\WithSignedData;

/**
 * The response we received, through a redirect to a pay button
 */
class PayButtonResponse implements WithSignedData, Response
{
	/**
	 * Status when the payment has been accepted
	 */
	const STATUS_ACCEPTED = 'ACCEPTED';

	/**
	 * Status when the payment has been canceled by the customer (end-user)
	 */
	const STATUS_CANCELLED = 'CANCEL';

	/**
	 * Status when the payment has been declined or something goes wrong
	 */
	const STATUS_DECLINED = 'DECLINED';

	/**
	 * the reference of *your* order
	 * @var string
	 */
	public $merchantOrderReference;

	/**
	 * The status of this payment
	 * @var string
	 */
	public $paymentStatus;

	/**
	 * The payment token, that can be reused to make other payment (during it's validity time)
	 * @var string|null
	 */
	public $paymentToken;

	public function __construct(string $merchantOrderReference, string $paymentStatus, ?string $paymentToken)
	{
		$this->merchantOrderReference = $merchantOrderReference;
		$this->paymentStatus = $paymentStatus;
		$this->paymentToken = $paymentToken;
	}

	public function getDataNameToSign(): array
	{
		return [
			'reference',
			'status',
		];
	}

	public function getData(): array
	{
		return [
			'reference' => $this->merchantOrderReference,
			'status' => $this->paymentStatus,
		];
	}

	public function getDirection(): string
	{
		return WithSignedData::SIGN_FROM_MONIZZE;
	}

	public function getMerchantOrderReference(): string
	{
		return $this->merchantOrderReference;
	}

	public function getPaymentStatus(): string
	{
		return $this->paymentStatus;
	}

	public function isPaymentStatusAccepted(): bool
	{
		return $this->getPaymentStatus() == self::STATUS_ACCEPTED;
	}

	public function getPaymentToken(): ?string
	{
		return $this->paymentToken;
	}
}
