<?php

namespace MonizzeLib\Payment\Message;

use MonizzeLib\Payment\Client;
use MonizzeLib\Payment\Contracts\Message\Request;
use MonizzeLib\Payment\Contracts\Message\ResponseToRequest;
use MonizzeLib\Payment\Contracts\Message\WithSignedData;
use Psr\Http\Message\ResponseInterface;

/**
 * This will request a payment with a token previously getted
 */
class PayWithTokenRequest implements Request
{
	protected const URL_PART = 'api/payment/pay';

	/**
	 * The reference of the order
	 * @var string
	 */
	protected $orderReference;

	/**
	 * The amount to pay express in eurocent
	 * @var int
	 */
	protected $amountInEurocent;

	/**
	 * @see constant in \MonizzeLib\Payment\Container\PayButton::*
	 * @var int
	 */
	protected $typeOfVoucher;

	/**
	 * The token of the user to pay with
	 * @var string
	 */
	protected $token;

	/**
	 * The language of the user
	 * @var string
	 */
	protected $language;

	/**
	 * Email of the customer
	 * @var string
	 */
	protected $customerEmail;

	/**
	 * @var string
	 */
	protected $shopName;

	public function __construct(string $orderReference, int $amountInEurocent, int $typeOfVoucher, string $token, string $language, string $customerEmail, string $shopName)
	{
		$this->orderReference = $orderReference;
		$this->amountInEurocent = $amountInEurocent;
		$this->typeOfVoucher = $typeOfVoucher;
		$this->token = $token;
		$this->language = $language;
		$this->customerEmail = $customerEmail;
		$this->shopName = $shopName;
	}

	public function getDataNameToSign(): array
	{
		return [
			Client::PARAM_MID,
			'MREF',
			'AMOUNT',
			'TYPE',
			'SHOP',
		];
	}

	public function getData(): array
	{
		return [
			Client::PARAM_MID => Client::PARAM_MID,
			'MREF' => $this->orderReference,
			'AMOUNT' => $this->amountInEurocent,
			'TYPE' => $this->typeOfVoucher,
			'LANGUAGE' => $this->language,
			'EMAIL' => $this->customerEmail,
			'SHOP' => $this->shopName,
			Client::PARAM_SIGN => Client::PARAM_SIGN,
			'TOKEN' => $this->token,
		];
	}

	public function getEndpoint(): string
	{
		return self::URL_PART;
	}

	public function createResponse(ResponseInterface $response): ResponseToRequest
	{
		return PayWithTokenResponse::createFromResponse($response);
	}

	public function getDirection(): string
	{
		return WithSignedData::SIGN_TO_MONIZZE;
	}
}
