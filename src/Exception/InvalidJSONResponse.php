<?php

namespace MonizzeLib\Payment\Exception;

class InvalidJSONResponse extends \DomainException
{
	protected $response;

	public function __construct(string $message, string $response)
	{
		parent::__construct($message);
		$this->response = $response;
	}

	public function getResponse(): string
	{
		return $this->response;
	}
}
