<?php

namespace MonizzeLib\Payment\Exception;

class InvalidXmlResponse extends \DomainException
{
	/**
	 * @var array
	 */
	protected $errors;

	public function __construct(string $message, array $errors)
	{
		parent::__construct($message);
		$this->errors = $errors;
	}

	/**
	 * @return array
	 */
	public function getErrors(): array
	{
		return $this->errors;
	}
}
