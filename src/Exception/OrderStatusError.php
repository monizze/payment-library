<?php

namespace MonizzeLib\Payment\Exception;

class OrderStatusError extends \DomainException
{
	const ORDER_NOT_FOUND = 'ORDER_NOT_FOUND';
	const DATA_INTEGRITY_FAILED = 'DATA_INTEGRITY_FAILED';
	const MERCHANT_NOT_FOUND = 'MERCHANT_NOT_FOUND';
	const FORM_DATA_MISSING = 'FORM_DATA_MISSING';
	const NO_POST_DATA = 'NO_POST_DATA';
	
	protected $type;
	
	public function __construct(string $type)
	{
		parent::__construct('An error occurs on payment status request');
		$this->type = $type;
	}
	
	public function getType(): string
	{
		return $this->type;
	}
}
