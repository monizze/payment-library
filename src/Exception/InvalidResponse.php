<?php

namespace MonizzeLib\Payment\Exception;

use Psr\Http\Message\ResponseInterface;

class InvalidResponse extends \DomainException
{
	/**
	 * @var ResponseInterface
	 */
	protected $response;

	public function __construct(string $message, ResponseInterface $response)
	{
		parent::__construct($message, $response->getStatusCode());
		$this->response = $response;
	}

	public function getResponse(): ResponseInterface
	{
		return $this->response;
	}
}
