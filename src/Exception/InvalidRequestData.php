<?php

namespace MonizzeLib\Payment\Exception;

class InvalidRequestData extends \DomainException
{
	public function __construct(string $message)
	{
		parent::__construct($message);
	}
}
