<?php

namespace MonizzeLib\Payment\Exception;

class OrderStatusInvalid extends \DomainException
{
	public function __construct(string $message)
	{
		parent::__construct($message);
	}
}
