<?php

namespace tests\units\MonizzeLib\Payment\Container;

class PayButton extends \atoum\test
{
	public function testConstructorAndDefaultValue()
	{
		$this->newTestedInstance($order = uniqid(), 4200, 1234);
		$this->string($this->testedInstance->merchantOrderReference)->isIdenticalTo($order);
		$this->integer($this->testedInstance->amountInEurocent)->isIdenticalTo(4200);
		$this->integer($this->testedInstance->typeOfVoucher)->isIdenticalTo(1234);
		$this->string($this->testedInstance->shopName)->isEmpty;
		$this->string($this->testedInstance->contactEmail)->isEmpty;
		$this->string($this->testedInstance->urlOfPaymentAccepted)->isEmpty;
		$this->string($this->testedInstance->urlOfPaymentDeclined)->isEmpty;
		$this->string($this->testedInstance->urlOfPaymentCanceled)->isEmpty;
	}
}
