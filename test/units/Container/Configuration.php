<?php

namespace tests\units\MonizzeLib\Payment\Container;

class Configuration extends \atoum\test
{
	public function testConstructor($keyIn, $keyOut, $merchantId, $algo) {
		$this->newTestedInstance($keyIn, $keyOut, $merchantId, $algo);
		$this->string($this->testedInstance->getKeyIn())->isIdenticalTo($keyIn);
		$this->string($this->testedInstance->getKeyOut())->isIdenticalTo($keyOut);
		$this->integer($this->testedInstance->getMerchantId())->isIdenticalTo($merchantId);
		$this->string($this->testedInstance->getSignAlgo())->isIdenticalTo($algo);
	}

	protected function testConstructorDataProvider() {
		return [
			'normal' => [
				'my key in',
				'my key out',
				1234,
				'sha1'
			],
		];
	}
}
