<?php

namespace tests\units\MonizzeLib\Payment;

use MonizzeLib\Payment\Container\Configuration;
use MonizzeLib\Payment\Contracts\AdapterPsr\ClientInterface;
use MonizzeLib\Payment\Contracts\Message\WithSignedData;

class Client extends \atoum\test
{
	public function beforeTestMethod($method)
	{
		switch ($method)
		{
			case 'testSignData':
				$this->getMockGenerator()->orphanize('__construct');
				break;
		}
	}

	public function testSignData(Configuration $configuration, ClientInterface $httpClient, WithSignedData $signedData)
	{
		$this->newTestedInstance($configuration, $httpClient);
		$this->calling($signedData)->getDataNameToSign = [];
		$this->calling($signedData)->getDirection = WithSignedData::SIGN_FROM_MONIZZE;
		$this->calling($configuration)->getKeyOut = 'some key';
		$this->calling($signedData)->getData = array();
		$this->function->hash = function($algo, $values)
		{
			$this->string($algo)->isEqualTo('sha1');
			$this->string($values)->isEqualTo('some key');
			return 'bar';
		};
		$this->string($this->testedInstance->signData($signedData, array()))->isEqualTo('bar');
	}
}
