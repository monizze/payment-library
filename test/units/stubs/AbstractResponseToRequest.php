<?php

namespace stubs;

use MonizzeLib\Payment\Contracts\Message\ResponseToRequest;
use MonizzeLib\Payment\Message\AbstractResponseToRequest as ARTR;
use Psr\Http\Message\ResponseInterface;

// AbstractResponseToRequest is abstract so we need to create an empty class for it ;)

class AbstractResponseToRequest extends ARTR
{
	public static function createFromResponse(ResponseInterface $response): ResponseToRequest
	{
		// XXX do nothing, we don't care about this
	}
}
