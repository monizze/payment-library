<?php

namespace tests\units\MonizzeLib\Payment\Message;

use MonizzeLib\Payment\Contracts\Message\ResponseToRequest;
use MonizzeLib\Payment\Exception\InvalidResponse;
use MonizzeLib\Payment\Exception\OrderStatusInvalid;
use MonizzeLib\Payment\Message\PayButtonResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

class OrderStatusResponse extends \atoum\test
{
	public function testXmlToResponse(string $dom, string $status, string $returnedStatus)
	{
		$dom = simplexml_load_string($dom);
		$this->newTestedInstance;
		$this->variable($this->testedInstance->xmlToResponse($dom))->isNull;
		$this->string($this->testedInstance->getStatus())
			->isEqualTo($status);
		$this->string($this->testedInstance->getReturnedStatus())
			->isEqualTo($returnedStatus);
	}

	public function testXmlToResponseNoStatus()
	{
		$this->newTestedInstance;
		$this->exception(function()
		{
			$dom = simplexml_load_string('<foo><something>status</something></foo>');
			$this->testedInstance->xmlToResponse($dom);
		})
			->isInstanceOf(OrderStatusInvalid::class)
			->hasMessage('No status provided');
	}

	public function testXmlToResponseNotAnAcceptableStatus()
	{
		$this->newTestedInstance;
		$this->exception(function()
		{
			$dom = simplexml_load_string('<foo><status>something</status></foo>');
			$this->testedInstance->xmlToResponse($dom);
		})
			->isInstanceOf(OrderStatusInvalid::class)
			->hasMessage('The status is not matchable with any known status');
	}

	public function testCreateFromResponse()
	{
		$stream = $this->newMockInstance(StreamInterface::class);
		$response = $this->newMockInstance(ResponseInterface::class);
		$this->calling($response)->getStatusCode = 200;
		$this->calling($response)->getBody = $stream;
		$this->calling($stream)->rewind = true;
		$this->calling($stream)->getContents = '<foo><status>ACCEPTED</status></foo>';

		$this->assert('Test valid response');
		$class = $this->testedClass->getClass();
		$this->object($orderResponse = $class::createFromResponse($response))
			->isInstanceOf(ResponseToRequest::class)
			->isInstanceOf($class);
		$this->string($orderResponse->getStatus())->isEqualTo(PayButtonResponse::STATUS_ACCEPTED);

		$this->assert('Test invalid response');
		$this->calling($response)->getStatusCode = 404;
		$this->exception(function() use ($class, $response)
		{
			$class::createFromResponse($response);
		})
			->isInstanceOf(InvalidResponse::class)
			->hasMessage('Invalid response');
		$this->object($this->exception->getResponse())->isIdenticalTo($response);
	}

	protected function testXmlToResponseDataProvider()
	{
		return [
			'initialized' => ['<foo><status>INITIALIZED</status></foo>', PayButtonResponse::STATUS_CANCELLED, 'INITIALIZED'],
			'cancelled' => ['<foo><status>CANCELLED</status></foo>', PayButtonResponse::STATUS_CANCELLED, 'CANCELLED'],
			'accepted' => ['<foo><status>ACCEPTED</status></foo>', PayButtonResponse::STATUS_ACCEPTED, 'ACCEPTED'],
			'declined' => ['<foo><status>DECLINED</status></foo>', PayButtonResponse::STATUS_DECLINED, 'DECLINED'],
		];
	}
}
