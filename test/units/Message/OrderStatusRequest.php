<?php

namespace tests\units\MonizzeLib\Payment\Message;

use MonizzeLib\Payment\Contracts\Message\WithSignedData;

class OrderStatusRequest extends \atoum\test
{
	public function testSimple()
	{
		$this->newTestedInstance($ref = uniqid(), 4200);
		$this->string($this->testedInstance->getDirection())->isEqualTo(WithSignedData::SIGN_TO_MONIZZE);
		$this->string($this->testedInstance->getEndPoint())->isEqualTo('online/pay/order-status');
		$this->array($this->testedInstance->getDataNameToSign())
			->hasSize(3);
		$this->array($this->testedInstance->getData())
			->hasSize(3);
	}
}
