<?php

namespace tests\units\MonizzeLib\Payment\Message;

use MonizzeLib\Payment\Contracts\Message\WithSignedData;

class PayButtonResponse extends \atoum\test
{
	public function testSimpleAccessor()
	{
		$this->newTestedInstance($order = uniqid(), $this->testedClass->getClass()::STATUS_ACCEPTED, null);
		$this->string($this->testedInstance->getMerchantOrderReference())->isIdenticalTo($order);
		$this->string($this->testedInstance->getPaymentStatus())->isEqualTo($this->testedClass->getClass()::STATUS_ACCEPTED);
		$this->variable($this->testedInstance->getPaymentToken())->isNull;
		$this->boolean($this->testedInstance->isPaymentStatusAccepted())->isTrue;

		$this->newTestedInstance($order = uniqid(), $this->testedClass->getClass()::STATUS_DECLINED, $token = uniqid());
		$this->string($this->testedInstance->getMerchantOrderReference())->isIdenticalTo($order);
		$this->string($this->testedInstance->getPaymentStatus())->isEqualTo($this->testedClass->getClass()::STATUS_DECLINED);
		$this->string($this->testedInstance->getPaymentToken())->isIdenticalTo($token);
		$this->boolean($this->testedInstance->isPaymentStatusAccepted())->isFalse;

		$this->string($this->testedInstance->getDirection())->isEqualTo(WithSignedData::SIGN_FROM_MONIZZE);
	}
}
