<?php

namespace tests\units\MonizzeLib\Payment\Message;

use MonizzeLib\Payment\Container\PayButton;
use MonizzeLib\Payment\Contracts\Message\WithSignedData;

class PayWithTokenRequest extends \atoum\test
{
	public function testSimple()
	{
		$this->newTestedInstance($ref = uniqid(), 4200, PayButton::GIFT, $token = sha1(uniqid()), 'fr', '', '');
		$this->string($this->testedInstance->getDirection())->isEqualTo(WithSignedData::SIGN_TO_MONIZZE);
		$this->string($this->testedInstance->getEndPoint())->isEqualTo('api/payment/pay');
		$this->array($this->testedInstance->getDataNameToSign())
			->hasSize(5);
		$this->array($this->testedInstance->getData())
			->hasSize(9);
	}
}
