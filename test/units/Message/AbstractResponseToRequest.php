<?php

namespace tests\units\stubs;

use MonizzeLib\Payment\Exception\InvalidXmlResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

class AbstractResponseToRequest extends \atoum\test
{
	public function testResponseValidity()
	{
		$stream = $this->newMockInstance(StreamInterface::class);
		$response = $this->newMockInstance(ResponseInterface::class);
		$this->calling($response)->getStatusCode = 200;
		$this->calling($response)->getBody = $stream;
		$this->calling($stream)->rewind = true;
		$this->calling($stream)->getContents = 'foo';

		$this->newTestedInstance;
		$this->assert('Everything works fine')
			->boolean($this->testedInstance->isResponseValid($response))
				->isTrue;

		$this->calling($response)->getStatusCode = 404;
		$this->assert('Not a 200 response')
			->boolean($this->testedInstance->isResponseValid($response))
			->isFalse;

		$this->calling($response)->getStatusCode = 200;
		$this->calling($stream)->getContents = '';
		$this->assert('Empty response')
			->boolean($this->testedInstance->isResponseValid($response))
			->isFalse;
	}

	public function testExtractContentFromResponse()
	{
		$stream = $this->newMockInstance(StreamInterface::class);
		$response = $this->newMockInstance(ResponseInterface::class);
		$this->calling($response)->getBody = $stream;
		$this->calling($stream)->rewind = true;
		$this->calling($stream)->getContents = 'foo';

		$this->newTestedInstance;
		$this->string($this->testedInstance->extractContentFromResponse($response))
			->isEqualTo('foo');
	}

	public function testResponseToXml()
	{
		$this->newTestedInstance;

		$this->assert('Valid xml extractor')
			->object($dom = $this->testedInstance->responseToXml('<?xml version="1.0" encoding="UTF-8"?><stuff><foo>bar</foo></stuff>'))
				->isInstanceOf(\SimpleXMLElement::class)
			->string('' . $dom->foo)->isEqualTo('bar');

		$this->assert('Invalid xml extractor')
			->exception(function()
			{
				$this->object($this->testedInstance->responseToXml('<?xml version="1.0" encoding="UTF-8"?><stuff>'));
			})
			->isInstanceOf(InvalidXmlResponse::class)
			->hasMessage('Impossible to read correctly the response');
		$this->array($this->exception->getErrors());
	}
}
