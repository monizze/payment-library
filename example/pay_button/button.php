<?php

require_once __DIR__ . '/inc/boot.php';

use MonizzeLib\Payment\Container\PayButton;
use MonizzeLib\Payment\Client;

// the language of the customer
// XXX can be 'fr', 'nl' or 'en'
$language = 'en';

// first we select something to purchase
if (!isset($_GET['type'])):
?>
<!doctype html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
</head>
<body>
	<p>
		What do you want to pay?
	</p>
	<ul>
		<li><a href="?type=meal">A meal</a></li>
		<li><a href="?type=eco">Some ecological product</a></li>
		<li><a href="?type=gift">Something you can purchase with your gift</a></li>
	</ul>
</body>
</html>
<?php
else:

$type = smallCleanupInput('type');
$key = $type . '-' . time();

// save the correct order code
// here we use session as an example
$_SESSION['orders'][ $key ] = [
	'type' => $type,
	'ref' => $key,
];

switch ($type)
{
	default:
	case 'meal':
		$_SESSION['orders'][ $key ]['code'] = PayButton::EMV;
		$_SESSION['orders'][ $key ]['amount'] = 399; // 3,99€
		break;
	case 'eco':
		$_SESSION['orders'][ $key ]['code'] = PayButton::ECO;
		$_SESSION['orders'][ $key ]['amount'] = 14999; // 149,99€
		break;
	case 'gift':
		$_SESSION['orders'][ $key ]['code'] = PayButton::GIFT;
		$_SESSION['orders'][ $key ]['amount'] = 3000; // 30,00€
		break;
}

$button = new PayButton($_SESSION['orders'][ $key ]['ref'], $_SESSION['orders'][ $key ]['amount'], $_SESSION['orders'][ $key ]['code']);

// ---------------- start optional
// now we add extra optional information, by default if it's not provided it will be taken from the configuration on Monizze side
// XXX these url should be build, here as an easy demo we generate them
// note on theses url, can have existing query parameters without any issue
$button->urlOfPaymentAccepted = $_SERVER['HTTP_HOST'] . dirname($_SERVER['REQUEST_URI']) . 'redirect.php';
$button->urlOfPaymentDeclined = $_SERVER['HTTP_HOST'] . dirname($_SERVER['REQUEST_URI']) . 'redirect.php';
$button->urlOfPaymentCanceled = $_SERVER['HTTP_HOST'] . dirname($_SERVER['REQUEST_URI']) . 'redirect.php';

// we can also override some interesting value
$button->contactEmail = 'user@example.tld';
$button->shopName = 'The best demo shop';
// ---------------- end optional

// create the signature of the data we send
$btnData = $monizzeClient->fillRequestData($button);
$signature = $monizzeClient->signData($button, $btnData);
?>
<!doctype html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
</head>
<body>
<form method="POST" action="https://www.monizze.be/<?= $language ?>/online/pay/">
	<p>
		Pay your order ("<?= $type ?>")?<br />
		<strong><?= number_format($button->amountInEurocent / 100, 2) ?>€</strong>
	</p>
	<input type="hidden" name="MID" value="<?= $btnData[ Client::PARAM_MID ] ?>" />
	<input type="hidden" name="MREF" value="<?= $button->merchantOrderReference ?>" />
	<input type="hidden" name="AMOUNT" value="<?= $button->amountInEurocent ?>">
	<input type="hidden" name="TYPE" value="<?= $button->typeOfVoucher ?>" />
	<input type="hidden" name="EMAIL" value="<?= $button->contactEmail ?>" />
	<input type="hidden" name="SHOP" value="<?= $button->shopName ?>" />
	<input type="hidden" name="URL_ACCEPTED" value="<?= $button->urlOfPaymentAccepted ?>">
	<input type="hidden" name="URL_CANCELED" value="<?= $button->urlOfPaymentCanceled ?>">
	<input type="hidden" name="URL_DECLINED" value="<?= $button->urlOfPaymentDeclined ?>">
	<input type="hidden" name="SIGN" value="<?= $signature ?>">
	<button type="submit">
		Pay with Monizze<br />
		<img src="https://www.monizze.be/content/themes/monizze/dist/img/logo_monizze.svg" />
	</button>
</form>
</body>
</html>
<?php endif; ?>
