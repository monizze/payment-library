<?php

session_start();

// autoloader
include_once __DIR__ . '/../../../vendor/autoload.php';

// create the HTTP adapter to make the HTTP call to the API
$HTTPAdapter = new \MonizzeLib\Payment\AdapterPsr\Guzzle6Adapter();

// load configuration
$config = require __DIR__ . '/config.php';

// create the configuration container
$configuration = new \MonizzeLib\Payment\Container\Configuration($config[ 'key_in'], $config[ 'key_out'], $config[ 'mid'], $config[ 'algo']);

// create the Monizze client
$monizzeClient = new \MonizzeLib\Payment\Client($configuration, $HTTPAdapter);

// store our order ;) You would probably use the database
if (!isset($_SESSION['orders']))
{
	$_SESSION['orders'] = array();
}

// XXX we have no framework, but we still need to cleanup a bit the inputs
// this is very basic, but at least it avoid some issue
function smallCleanupInput($paramName)
{
	global $_GET;
	if (!isset($_GET[ $paramName ]))
	{
		return null;
	}

	$data = trim($_GET[ $paramName ]);
	$data = addslashes($data);
	$data = htmlentities($data);

	return $data;
}
