<?php

return [
	// provided by Monizze
	'mid' => 42,
	'key_in' => 'your key in',
	'key_out' => 'your key out',

	// your choice amongs all the possibility provided by Monizze
	// sha1, sha256 or sha512
	'algo' => 'sha512',
];
