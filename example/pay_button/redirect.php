<?php

require_once __DIR__ . '/inc/boot.php';

use MonizzeLib\Payment\Message\OrderStatusRequest;
use MonizzeLib\Payment\Message\PayButtonResponse;
use MonizzeLib\Payment\Message\OrderStatusResponse;

// check the parameter of the page
$params = ['MREF', 'STATUS', 'SIGN', 'TOKEN'];
$inputs = array();
foreach ($params as $param)
{
	$inputs[ $param ] = smallCleanupInput($param);
}

if (empty($inputs['MREF']) || empty($inputs['STATUS']) || empty($inputs['SIGN']))
{
	echo 'No valid data';
	exit();
}

// let's validate the data we receive are OK
$response = new PayButtonResponse($inputs['MREF'], $inputs['STATUS'], $inputs['TOKEN']);
if (!$monizzeClient->validateSignatureOnSignedData($inputs['SIGN'], $response))
{
	echo 'The return from Monizze is invalid.';
	exit();
}

// we check that we have theses data in our side
// probably a database call or whataver ...
$order = $_SESSION['orders'][ $inputs['MREF'] ] ?? null;
if (!$order)
{
	echo 'We didn\'t found any order matching your payement. Contact us.';
	exit();
}

$status = $response->getPaymentStatus();

// ----------------- start optional
// To be certain about the validity of the status, we call back Monizze on this (*optional*)
$orderStatusRequest = new OrderStatusRequest($inputs['MREF'], $order['amount']);
/* @var OrderStatusResponse $statusResponse */
$statusResponse = $monizzeClient->doRequest($orderStatusRequest);
$status = $statusResponse->getStatus();
// ----------------- end optional

switch ($status)
{
	case PayButtonResponse::STATUS_ACCEPTED:
		echo 'Congrats, your order is payed!';
		break;
	case PayButtonResponse::STATUS_CANCELLED:
		echo 'You cancel the payment. Don\'t you want to order it?';
		break;
	case PayButtonResponse::STATUS_DECLINED:
		echo 'Ouch your payment has been declined! It can be a technical error (retry) or you didn\'t have enough voucher to pay.';
		break;
}

// XXX now you can mark the order has processed
