# Pay button with monizze

This documents are an example of how to implements a pay button with monizze and get the answer from the monizze server.

## Run the demo!

* Run ```composer install``` in the root of the project
* Change the configuration of the file `inc/config.php`
* Deploy the files on a webserver
* Go to the corresponding url on the `button.php` page.
* Once you have payed, you will be redirected to the `redirect.php` page.

### Or you can run it locally with php

```
cd example/pay_button
php -S localhost:8080 -t .
```

Then go to `http://localhost:8080/button.php`.

## Note
This example is a base to work with and required to be adapted to your framework, cms, ...
